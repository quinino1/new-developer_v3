## Info about OS and experience

I use RTFM, a few days ago, when I was implementing an authentication system using a new library for Django. Before searching the internet I prefer to read the documentation to see its configuration and what other features I can use for my project.

And about LMGTFY i use it everyday, i use it to get more information about certain libraries. I think that nowdays it s imposible to not use Google for work. 

Currently im working in a Windodows PC, but i use WSL2(Ubuntu), While i was a student, I used ArchLinux a lot so i know how to use Google to see if some else have the same problem and how to solve it.

My programming languages are:
- Python
- JavaScript
- Golang(Basic)
- Rust(Basic)

I like to learn new languages and see how can i use it ion my work.