# Use an official Python runtime as the base image
FROM python:3.11

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY ./src /app

# Install any needed dependencies specified in requirements.txt
# For a simple script, this may not be necessary
RUN pip install -r requirements.txt

# Define the command to run your script
CMD ["python", "main.py"]
