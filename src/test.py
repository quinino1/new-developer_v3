import unittest
from main import find_pair_sum

class TestMain(unittest.TestCase):
    """
    Tests for main.two_sum
    """
    def test_two_sum_ok(self):
        """Successful"""
        nums = [2,3,6,7]
        target = 9
        expected = (2, 7)
        self.assertEqual(find_pair_sum(nums, target), expected)

    def test_two_sum_nil(self):
        """Not found """
        nums = [1,3,3,7]
        target = 9
        expected = None
        self.assertEqual(find_pair_sum(nums, target), expected)

if __name__ == '__main__':
    unittest.main()
