"""
Solution for quetion #4 of RebajaTusCuentas
"""


def find_pair_sum(nums, target_sum):
    """
    Finds a pair in a sorted array whose sum equals a target sum.

    Args:
        nums: A list of numbers in ascending order.
        target_sum: The target sum to find a pair for.

    Returns:
        A tuple containing the two elements of the pair, or None if no pair is found.
    """

    left = 0
    right = len(nums) - 1

    while left < right:
        current_sum = nums[left] + nums[right]

        if current_sum == target_sum:
            return nums[left], nums[right]
        elif current_sum < target_sum:
            left += 1
        else:
            right -= 1

    return None


if __name__ == "__main__":
    arr = [2, 3, 6, 7]
    target = 9

    pair = find_pair_sum(arr, target)

    if pair:
        print("OK, matching pair ", pair)
    else:
        print("No")
